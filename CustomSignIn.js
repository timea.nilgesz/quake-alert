import { useState } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';
import { useAuthenticator } from '@aws-amplify/ui-react-native';

const CustomSignIn = () => {
    const { signIn } = useAuthenticator();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleSignIn = async () => {
        try {
            await signIn({ username, password });
        } catch (error) {
            console.error('Error signing in:', error);
        }
    };

    return (
        <View style={styles.container}>
            <TextInput
                value={username}
                onChangeText={setUsername}
                placeholder="Username"
                style={styles.input}
            />
            <TextInput
                value={password}
                onChangeText={setPassword}
                placeholder="Password"
                secureTextEntry
                style={styles.input}
            />
            <Button title="Sign In" color="#FFA500" onPress={handleSignIn} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        marginBottom: 12,
        borderWidth: 1,
        padding: 10,
    },
});

export default CustomSignIn;
