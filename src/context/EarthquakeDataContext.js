// src/contexts/EarthquakeDataContext.js
import React, { createContext, useState, useContext } from 'react';

const EarthquakeDataContext = createContext();

export const useEarthquakeData = () => useContext(EarthquakeDataContext);

export const EarthquakeDataProvider = ({ children }) => {
  const [earthquakeData, setEarthquakeData] = useState(null);

  return (
    <EarthquakeDataContext.Provider value={{ earthquakeData, setEarthquakeData }}>
      {children}
    </EarthquakeDataContext.Provider>
  );
};
