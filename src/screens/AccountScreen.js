import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, Switch, Alert, Image, Platform, Button, TextInput, TouchableOpacity } from 'react-native';
import { check, request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
import { generateClient } from '@aws-amplify/api';
import { onSensorDataUpdated } from '../graphql/subscriptions';
import { useAuthenticator } from '@aws-amplify/ui-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { useEarthquakeData } from '../context/EarthquakeDataContext';

const client = generateClient();

const degreesToRadians = (degrees) => degrees * Math.PI / 180;



const calculateDistance = (coords1, coords2) => {
  const earthRadiusKm = 6371;
  const dLat = degreesToRadians(coords2.latitude - coords1.latitude);
  const dLon = degreesToRadians(coords2.longitude - coords1.longitude);
  const lat1 = degreesToRadians(coords1.latitude);
  const lat2 = degreesToRadians(coords2.latitude);
  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return earthRadiusKm * c;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#292929',
    paddingTop: Platform.OS === 'android' ? 25 : 0,
  },
  gpsContainer: {
    position: 'absolute',
    top: 10,
    left: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  gpsText: {
    fontSize: 18,
    color: '#ffffff',
    marginRight: 10,
  },
  alertContainer: {
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    width: '90%',
    borderRadius: 10,
    marginVertical: 20,
    backgroundColor: '#f48c06',
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 5,
    color: "#292929"
  },
  infoText: {
    fontSize: 18,
    marginBottom: 5,
    color: "#292929"
  },
  actionImage: {
    width: '100%',
    height: undefined,
    aspectRatio: 1.5,
    resizeMode: 'contain',
  },
  settingsPanel: {
    marginTop: 20,
    backgroundColor: '#333333',
    padding: 10,
    borderRadius: 5,
    width: '90%',
  },
  gpsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  gpsText: {
    color: '#ffffff',
    marginRight: 10,
    fontSize: 16,
  },
  input: {
    height: 40,
    margin: 0,
    color: '#ffffff',
    fontSize: 16,
  },
  settingsButtonContainer: {
    flex: 1,
    alignSelf: 'flex-start', // Aligns the container to the left
    padding: 20, // Add some padding for visual appeal
    width: '50%', // Take full width to ensure alignment is effective
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%', // This ensures the container takes full width
    marginTop: 10,
  },
  signOutButtonContainer: {
    // Add necessary styling here
    flex:1,
    alignItems: 'flex-end',
    paddingRight: 20,  // Add padding to ensure there is tap space around the icon
  },
  signOutIcon: {
    width: 30,    // Set width of the icon
    height: 30,   // Set height of the icon
    resizeMode: 'contain' // This ensures the icon scales correctly within the button area
  }
});

const AccountScreen = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const [coordinates, setCoordinates] = useState(null);
  const { earthquakeData, setEarthquakeData } = useEarthquakeData();
  const [showSettings, setShowSettings] = useState(false);
  const [notificationDistance, setNotificationDistance] = useState('0'); // Default distance in kilometers
  
  
  const { signOut } = useAuthenticator();
  let watchId = null;  // Declare watchId in a broader scope


  const fetchLocation = useCallback(() => {
    const watchId = Geolocation.watchPosition(
      position => {
        setCoordinates(position.coords);
        setIsEnabled(true);
      },
      error => {
        console.error('Location fetching error:', error);
        setIsEnabled(false);
        setCoordinates(null);
      },
      { enableHighAccuracy: true, distanceFilter: 0, timeout: 20000, maximumAge: 10000 }
    );
  
    // Clean up the watcher when the component is unmounted
    return () => {
      if (watchId) {
        Geolocation.clearWatch(watchId);
      }
    };
  }, []);
  
  useEffect(() => {
    (async () => {
      const result = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      if (result === RESULTS.GRANTED) {
        setIsEnabled(true);
        fetchLocation();  // Start location updates
        console.log("granted")
      } else if (result === RESULTS.DENIED) {
        const requestResult = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
        if (requestResult === RESULTS.GRANTED) {
          setIsEnabled(true);
          fetchLocation();  // Start location updates
        } else {
          setIsEnabled(false);
        }
      } else if (result === RESULTS.BLOCKED) {
        Alert.alert(
          "Permission Blocked",
          "Location permission is blocked. Please enable it in settings.",
          [{ text: "Open Settings", onPress: () => openSettings() }]
        );
      }
    })();
  }, [fetchLocation]);
  
  

  useEffect(() => {

    const loadSettings = async () => {
      try {
          const savedDistance = await AsyncStorage.getItem('notificationDistance');
          if (savedDistance !== null) {
              setNotificationDistance(savedDistance);
              console.log("Loaded saved distance:", savedDistance);
          }
      } catch (error) {
          console.error("Failed to load saved distance:", error);
      }
    };

    const subscription = client.graphql({ query: onSensorDataUpdated }).subscribe({
      next: ({ data }) => {
        loadSettings()

        const earthquakeInfo = data.onSensorDataUpdated;
        if (earthquakeInfo) {

          const earthquakeCoords = earthquakeInfo.gps_location.split(', ').map(Number);
          const distance = calculateDistance(coordinates, { latitude: earthquakeCoords[0], longitude: earthquakeCoords[1] }).toFixed(2);
          if (notificationDistance === '0' || (notificationDistance && notificationDistance>distance)) {
            setEarthquakeData({
              intensity: `${parseFloat(earthquakeInfo.acceleration).toFixed(3)} m/s²`,
              distance: `${distance} km away`,
              type: earthquakeInfo.intensity,
              date: new Date(earthquakeInfo.timestamp).toLocaleDateString(),
              time: new Date(earthquakeInfo.timestamp).toLocaleTimeString(),
            });
          }
          
        }
      },
      error: error => console.error("Subscription error:", error),
      complete: () => console.log("Subscription complete"),
    });
    return () => subscription.unsubscribe();
  }, [coordinates, setEarthquakeData, notificationDistance]);



  const handleSaveSettings = async () => {
    const distanceValue = parseFloat(notificationDistance);
    if (!isNaN(distanceValue) && distanceValue >= 0) {
        try {
            // Convert to string for storage
            const stringDistance = distanceValue.toString();
            await AsyncStorage.setItem('notificationDistance', stringDistance);
            console.log("Distance saved successfully:", distanceValue);
            
            // Update state directly without a callback
            setNotificationDistance(stringDistance);

            setShowSettings(false);
            Alert.alert("Success", "Settings saved successfully.");
        } catch (error) {
            console.error("Failed to save distance:", error);
            Alert.alert("Error", "Failed to save settings.");
        }
    } else {
        Alert.alert("Invalid Input", "Please enter a valid number for the distance.");
    }
};




const toggleSwitch = async (newIsEnabled) => {
  if (!newIsEnabled) {
    setIsEnabled(false);
    Geolocation.clearWatch(); // Stop watching the location when disabled
    openSettings();  // Direct user to open settings to manually toggle location permission
  } else {
    try {
      const result = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

      if (result === RESULTS.GRANTED) {
        fetchLocation();  // Start watching the location
        setIsEnabled(true);
      } else if (result === RESULTS.BLOCKED) {
        Alert.alert(
          "Permission required",
          "Location permission is blocked. Please enable it in settings to continue.",
          [
            { text: "Open Settings", onPress: () => openSettings() },
            { text: "Cancel", style: "cancel" }
          ]
        );
      } else {
        setIsEnabled(false);
        Alert.alert(
          "Permission required",
          "Location permission is needed to fetch data.",
          [
            { text: "Open Settings", onPress: () => openSettings() },
            { text: "Cancel", style: "cancel" }
          ]
        );
      }
    } catch (error) {
      console.error("Permission request error:", error);
      setIsEnabled(false);
      Alert.alert("Error", "An error occurred while requesting location permissions.");
    }
  }
};

  const handleDismiss = () => {
    setEarthquakeData(null);
  };

  return (
    <View style={styles.container}>
      {/* <View style={styles.gpsContainer}>
        <Text style={styles.gpsText}>GPS Location</Text>
        <Switch
          trackColor={{ false: "gray", true: "gray" }}
          thumbColor={isEnabled ? "#FFA500" : "#f4f3f4"}
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </View> */}
      
      <View style={styles.buttonContainer}>
        <View style={styles.settingsButtonContainer}>
          <Button title="Settings" onPress={() => setShowSettings(!showSettings)} color="#FFA500" />
        </View>
        <View style={styles.signOutButtonContainer}>
          <TouchableOpacity onPress={signOut}>
            <Image
              source={require('../../assets/icons/logout.png')}  // Update path as needed
              style={styles.signOutIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
      {showSettings && (
        <View style={styles.settingsPanel}>
          <View style={styles.gpsContainer}>
            <Text style={styles.gpsText}>GPS Location</Text>
            <Switch
              trackColor={{ false: "gray", true: "gray" }}
              thumbColor={isEnabled ? "#FFA500" : "#f4f3f4"}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
          <TextInput
            style={styles.input}
            onChangeText={setNotificationDistance}
            value={notificationDistance}
            keyboardType="numeric"
            placeholder="Notification distance in km"
            placeholderTextColor={"gray"}
          />
          <Button title="Save Settings" onPress={handleSaveSettings} color="#FFA500" />
        </View>
      )}
      {earthquakeData && (
        <View style={styles.alertContainer}>
          <Text style={styles.header}>EARTHQUAKE ALERT</Text>
          <Text style={styles.header}>{earthquakeData.type}</Text>
          <Text style={styles.infoText}>Intensity: {earthquakeData.intensity}</Text>
          <Text style={styles.infoText}>{earthquakeData.distance}</Text>
          <Text style={styles.infoText}>{earthquakeData.date} {earthquakeData.time}</Text>
          <Image style={styles.actionImage} source={require('../resources/drop-cover-holdon.png')} />
          <Button
            title="Dismiss Alert"
            onPress={handleDismiss}
            color="#D9534F"  // Bootstrap's "danger" color for dismiss button
          />
        </View>
      )}
    </View>
  );
};

export default AccountScreen;
