import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, Image, Pressable, Modal } from 'react-native';
import { generateClient } from '@aws-amplify/api';
import { getAllSensorData } from '../graphql/queries';
import { onSensorDataUpdated } from '../graphql/subscriptions';
import DateTimePicker from '@react-native-community/datetimepicker';
import DropDownPicker from 'react-native-dropdown-picker';

const getLocationByModule = (moduleId) => {
  switch (moduleId) {
    case 'sensor1-esp':
      return 'str. Venus, Timisoara';
    case 'sensor2-esp':
      return 'str. Sulina, Timisoara';
    case 'sensor3-esp':
      return 'str. Nera, Timisoara';
    default:
      return 'Unknown Location'; // Fallback for any unrecognized module IDs
  }
};
const getColorByType = (type) => {
  switch (type) {
    case 'Light':
      return '#ffba08'; // Light Blue
    case 'Moderate':
      return '#f48c06'; // Lime Green
    case 'Strong':
      return '#e85d04'; // Yellow
    case 'Very Strong':
      return '#dc2f02'; // Gold
    case 'Severe':
      return '#d00000'; // Orange
    case 'Violent':
      return '#9d0208'; // Orange-Red
    case 'Extreme':
      return '#6a040f'; // Red
    default:
      return '#FFFFFF'; // Default to white if no match
  }
};

const EarthquakeItem = ({ type, magnitude, date, time, location }) => {
  const displayLocation = getLocationByModule(location); // Get the display location based on the module ID

  return (
    <View style={[styles.itemContainer, { backgroundColor: getColorByType(type) }]}>
      <View style={styles.topRow}>
        <View style={styles.leftColumn}>
          <Text style={styles.type}>{type.toUpperCase()}</Text>
          <Text style={styles.magnitude}>{magnitude}</Text>
        </View>
        <View style={styles.locationContainer}>
          <Text style={styles.location}>{displayLocation}</Text>
        </View>
      </View>
      <View style={styles.bottomRow}>
        <Text style={styles.dateTime}>
          <Text style={styles.dateIcon}>&#128197; {date}</Text>
          <Text style={styles.timeIcon}> &#128338; {time}</Text>
        </Text>
      </View>
    </View>
  );
};



const client = generateClient();

const PastEarthquakesScreen = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [dateFilter, setDateFilter] = useState('');
  const [locationFilter, setLocationFilter] = useState('');
  const [intensityFilter, setIntensityFilter] = useState(null);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [openLocation, setOpenLocation] = useState(false);

  const [locationItems, setLocationItems] = useState([
    { label: 'All Locations', value: '' },
    { label: 'str. Avram Iancu, Iermata Neagra', value: 'sensor1-esp' },
    { label: 'str. Principala, Iermata Neagra', value: 'sensor2-esp' },
    { label: 'str. Crisului, Iermata Neagra', value: 'sensor3-esp' },
  ]);

  // State for dropdown
  const [open, setOpen] = useState(false);
  const [items, setItems] = useState([
    { label: 'All Intensities', value: '' },
    { label: 'Light', value: 'Light' },
    { label: 'Moderate', value: 'Moderate' },
    { label: 'Strong', value: 'Strong' },
    { label: 'Very Strong', value: 'Very Strong' },
    { label: 'Severe', value: 'Severe' },
    { label: 'Violent', value: 'Violent' },
    { label: 'Extreme', value: 'Extreme' },
  ]);

  const fetchData = useCallback(async () => {
    try {
      const response = await client.graphql({ query: getAllSensorData });
      const items = response.data.getAllSensorData || [];
      const formattedData = items.map(item => ({
        id: item.package_id, // Use timestamp as a unique identifier
        type: item.intensity,
        magnitude: `${item.acceleration} M/S²`,
        date: new Date(item.timestamp).toLocaleDateString(),
        time: new Date(item.timestamp).toLocaleTimeString(),
        location: item.module_id,
      }));
      setData(formattedData);
      setFilteredData(formattedData);
    } catch (err) {
      console.error('Error fetching earthquake data:', err);
    }
  }, []);

  useEffect(() => {
    fetchData();  // Call initially to load data

    const subscription = client.graphql({ query: onSensorDataUpdated }).subscribe({
      next: ({ data }) => {
        fetchData();  // Refetch data whenever a new update is received
      },
      error: error => console.error('Subscription error:', error),
    });

    return () => subscription.unsubscribe();  // Cleanup subscription on component unmount
  }, [fetchData]);

  const applyFilters = () => {
    let filtered = data;

    if (dateFilter) {
      filtered = filtered.filter(item => item.date === dateFilter);
    }

    if (locationFilter) {
      filtered = filtered.filter(item => item.location.toLowerCase().includes(locationFilter.toLowerCase()));
    }

    if (intensityFilter) {
      filtered = filtered.filter(item => item.type.toLowerCase() === intensityFilter.toLowerCase());
    }

    setFilteredData(filtered);
  };

  const handleDateChange = (event, selectedDate) => {
    setShowDatePicker(false);
    if (selectedDate) {
      const formattedDate = selectedDate.toLocaleDateString(); // Format the date as MM/DD/YYYY
      setDateFilter(formattedDate);
    }
  };

  const resetFilters = () => {
    setDateFilter('');
    setLocationFilter('');
    setIntensityFilter('');
    setFilteredData(data);  // Reset filtered data to original data
  };
  
  

  return (
    <View style={styles.container}>
      <Text style={styles.header}>PAST EARTHQUAKES</Text>
      
      <View style={styles.filterContainer}>
      <TouchableOpacity onPress={() => setShowDatePicker(true)}>
        <View style={styles.filterInput}>
          <Text style={styles.filterText}>{dateFilter || 'Filter by Date (MM/DD/YYYY)'}</Text>
        </View>
      </TouchableOpacity>
      {showDatePicker && (
        <View style={styles.datePickerContainer}>
          <DateTimePicker
            value={new Date()}
            mode="date"
            display="default"
            onChange={handleDateChange}
            themeVariant="dark" // Android-specific prop
            textColor="#FFFFFF" // iOS-specific prop
          />
        </View>
      )}
        {/* Location Dropdown Picker */}
        <DropDownPicker
          open={openLocation}
          value={locationFilter}
          items={locationItems}
          setOpen={setOpenLocation}
          setValue={setLocationFilter}
          setItems={setLocationItems}
          style={styles.dropdownPicker}
          dropDownContainerStyle={styles.dropdownPickerContainer}
          textStyle={styles.filterText}
          placeholder="Select Location"
          placeholderStyle={{ color: "#CCCCCC" }} // Placeholder text color
          zIndex={1000} // Ensure dropdown is above other components
        />

        {/* Dropdown Picker */}
        <DropDownPicker
          open={open}
          value={intensityFilter}
          items={items}
          setOpen={setOpen}
          setValue={setIntensityFilter}
          setItems={setItems}
          style={styles.dropdownPicker}
          dropDownContainerStyle={styles.dropdownPickerContainer}
          textStyle={styles.filterText}
          placeholder="Select Intensity"
          placeholderStyle={{ color: "#CCCCCC" }} // Placeholder text color
          zIndex={1000} // Ensure dropdown is above other components
        />

        <View style={styles.filterButtonsContainer}>
          <TouchableOpacity 
            onPress={applyFilters} 
            style={styles.applyButton}
            activeOpacity={1} // Ensures no opacity change
          >
            <Text style={styles.applyButtonText}>Apply Filters</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={resetFilters} style={styles.resetButton}>
            <Image
              source={require('../../assets/icons/reset-settings.png')}  // Replace with the correct path to your image
              style={styles.resetIcon}
            />
          </TouchableOpacity>
        </View>
        
      </View>

      <FlatList
        data={filteredData}
        renderItem={({ item }) => <EarthquakeItem {...item} />}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#292929',
    padding: 20,
    paddingTop: Platform.OS === 'android' ? 25 : 20,
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
    color: '#FFFFFF', // White text
  },
  filterContainer: {
    marginBottom: 20,
  },
  filterButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center', // Center items horizontally
    alignItems: 'center',
    marginTop: 15,
  },
  filterInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
    justifyContent: 'center',
    backgroundColor: '#1A1A1A', // Dark background for inputs
  },
  filterText: {
    color: '#FFFFFF', // White text
  },
  dropdownPicker: {
    backgroundColor: '#1A1A1A', // Dark background for dropdown
    borderColor: '#ccc', // Border color for dropdown
  },
  dropdownPickerContainer: {
    backgroundColor: '#1A1A1A', // Dark background for dropdown container
  },
  itemContainer: {
    borderRadius: 20,
    padding: 15,
    marginBottom: 10,
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
  },
  type: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000000', // Black text for contrast with colored backgrounds
    textAlign: 'center',
  },
  magnitude: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000', // Black text for contrast with colored backgrounds
    textAlign: 'center',
    marginBottom: 5,
  },
  detailsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  details: {
    fontSize: 16,
    color: '#000000', // Black text for contrast with colored backgrounds
  },
  dateIcon: {
    fontWeight: 'bold',
  },
  timeIcon: {
    fontWeight: 'bold',
  },
  location: {
    fontSize: 16,
    color: '#000000', // Black text for contrast with colored backgrounds
    textAlign: 'center',
  },
  resetButton: {
    alignItems: 'center',
  },
  resetIcon: {
    width: 24,
    height: 24,
    tintColor: '#FFFFFF', // Ensure the reset icon is visible
  },
  applyButton: {
    backgroundColor: '#FFA500',
    padding: 10,
    width:'55%' ,
    borderRadius: 5,
    marginRight: 15,
    alignItems: 'center', // Center text horizontally
  },
  applyButtonText: {
    color: 'black', // Black text for contrast
  },

  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  leftColumn: {
    justifyContent: 'center',
  },
  rightColumn: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  leftColumn: {
    justifyContent: 'center',
  },
  rightColumn: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    flex: 1, // Allows wrapping of the location text if it is too long
  },
  type: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000000', // Black text for contrast with colored backgrounds
  },
  magnitude: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000', // Black text for contrast with colored backgrounds
  },
  location: {
    fontSize: 16,
    color: '#000000', // Black text for contrast with colored backgrounds
    textAlign: 'right',
    flexWrap: 'wrap', // Wraps the text if it's too long
  },
  bottomRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10, // Add some space above the date and time
  },
  dateTime: {
    fontSize: 16,
    color: '#000000', // Black text for contrast with colored backgrounds
  },
  dateIcon: {
    fontWeight: 'bold',
  },
  timeIcon: {
    fontWeight: 'bold',
  },
  locationContainer: {
    width: 150, // Fixed width for the location container
    alignItems: 'flex-end', // Align text to the right
  },
  datePickerContainer: {
    backgroundColor: '#333333', // Gray background
    borderRadius: 10, // Rounded corners for a nicer appearance
    padding: 10,
  },

  datePicker: {
    color: '#FFA500', // Orange color for selected date (iOS specific)
  },
});

export default PastEarthquakesScreen;
 