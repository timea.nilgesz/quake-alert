/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const addSensorData = /* GraphQL */ `
  mutation AddSensorData($data: SensorDataInput!) {
    addSensorData(data: $data) {
      package_id
      module_id
      gps_location
      acceleration
      timestamp
      intensity
      __typename
    }
  }
`;
