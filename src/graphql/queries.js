/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getAllSensorData = /* GraphQL */ `
  query GetAllSensorData {
    getAllSensorData {
      package_id
      module_id
      gps_location
      acceleration
      timestamp
      intensity
      __typename
    }
  }
`;
