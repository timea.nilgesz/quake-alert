# Earthquake Monitoring System - README

This documentation provides the necessary steps for building, debugging, and deploying the components of the Earthquake Monitoring System, which consists of:

1. **Earthquake React Amplify Mobile Application** (React Native)
2. **Raspberry Pi Code** (Node.js, managed by PM2)
3. **Arduino Code** (Running on NodeMCU boards)

Each section below outlines how to build, install, and run the respective component on the target devices.

---

## 1. Earthquake React Amplify Mobile Application

### Build Steps for Debugging

1. **Install Dependencies**:
   Ensure you have Node.js and React Native CLI installed on your machine. Run the following command to install dependencies:
   ```bash
   npm install
   ```

2. **Run the App for Debugging**:
   You can run the app in development mode using the following commands based on your platform:
   
   For iOS:
   ```bash
   npx react-native run-ios
   ```

   For Android:
   ```bash
   npx react-native run-android
   ```

   Ensure you have the appropriate emulator or physical device connected for debugging. The app will run in debug mode, allowing you to see logs and test functionality.

### Installation and Launch Steps (Debug)

- **For iOS**: The app will open in the iOS simulator after running the command. You can also deploy to a connected physical device by specifying the device in the `run-ios` command:
  ```bash
  npx react-native run-ios --device "Device Name"
  ```

- **For Android**: Similarly, the app will launch in the Android emulator or connected device. Ensure `adb` (Android Debug Bridge) recognizes the device by running:
  ```bash
  adb devices
  ```
  Then run the application as usual:
  ```bash
  npx react-native run-android
  ```

### Building and Installing APK for Release (Android)

1. **Generate a Signed APK**:
   To build a release version of the APK for Android, follow these steps:

   - First, generate a release Keystore:
     ```bash
     keytool -genkey -v -keystore earthquake-release-key.keystore -alias earthquakeKeyAlias -keyalg RSA -keysize 2048 -validity 10000
     ```

   - Configure the Keystore in `android/app/build.gradle` by adding the following:
     ```gradle
     signingConfigs {
         release {
             storeFile file('path/to/earthquake-release-key.keystore')
             storePassword 'your-password'
             keyAlias 'earthquakeKeyAlias'
             keyPassword 'your-key-password'
         }
     }

     buildTypes {
         release {
             signingConfig signingConfigs.release
         }
     }
     ```

2. **Build the APK**:
   Run the following command to create a release APK:
   ```bash
   cd android
   ./gradlew assembleRelease
   ```

3. **Install the APK on the Device**:
   After the APK is built, you can find it in the `android/app/build/outputs/apk/release` directory. Install the APK on your Android device:
   ```bash
   adb install app-release.apk
   ```

---

## 2. Raspberry Pi Code (Node.js)

### Application Build Steps

1. **Install Node.js Dependencies**:
   On the Raspberry Pi, navigate to the project directory and install the required npm dependencies:
   ```bash
   npm install
   ```

### Installation and Launch Steps (PM2)

1. **Install PM2**:
   Install PM2 globally to manage the Node.js application:
   ```bash
   npm install -g pm2
   ```

2. **Start the Application with PM2**:
   Use PM2 to start the application, which ensures it runs in the background:
   ```bash
   pm2 start index.js --name earthquake-monitor
   ```

3. **Enable Auto-Start on Boot**:
   Configure PM2 to run the application automatically on Raspberry Pi startup:
   ```bash
   pm2 startup
   ```

   Follow the instructions provided and run the generated command. Then, save the current process list:
   ```bash
   pm2 save
   ```

4. **Monitor Application**:
   To check the status of the application at any time:
   ```bash
   pm2 status
   ```

---

## 3. Arduino Code for NodeMCU Boards

### Application Build Steps

1. **Install Arduino IDE**:
   Ensure the Arduino IDE is installed on your computer. You can download it from the [official Arduino website](https://www.arduino.cc/en/software).

2. **Install the NodeMCU Board Libraries**:
   In the Arduino IDE, add the necessary support for NodeMCU (ESP8266/ESP32):
   - Go to `File > Preferences`
   - Add the following URL to the "Additional Boards Manager URLs":
     ```
     https://arduino.esp8266.com/stable/package_esp8266com_index.json
     ```
   - Go to `Tools > Board > Boards Manager` and install the `ESP8266` or `ESP32` package, depending on your NodeMCU model.

3. **Open the Arduino Code**:
   Open the `.ino` file in the Arduino IDE and make any necessary changes (e.g., Wi-Fi credentials).

4. **Configure Wi-Fi Credentials** (If applicable):
   Inside the Arduino code, update the Wi-Fi settings:
   ```cpp
   const char* ssid = "Your_SSID";
   const char* password = "Your_Password";
   ```

### Application Installation and Upload Steps

1. **Connect NodeMCU to Your Computer**:
   Use a USB cable to connect the NodeMCU board to your computer.

2. **Select the Board and Port**:
   - Go to `Tools > Board` and select the appropriate NodeMCU board (e.g., `NodeMCU 1.0 (ESP-12E Module)` for ESP8266).
   - Then, under `Tools > Port`, select the correct port corresponding to the connected device.

3. **Upload the Code**:
   Once the board and port are set, click the "Upload" button (the right-arrow icon) in the Arduino IDE to flash the code onto the NodeMCU.

4. **Monitor Serial Output**:
   After uploading, open the Serial Monitor (`Tools > Serial Monitor`) to confirm the device connects to Wi-Fi and starts transmitting data. Ensure the baud rate in the Serial Monitor matches the one set in your code.

---

## Conclusion

This document outlines the build, installation, and deployment steps for the Earthquake Monitoring System, including the React Native mobile app, Raspberry Pi Node.js service, and NodeMCU Arduino firmware. Follow the respective instructions for each component to set up and run the system.