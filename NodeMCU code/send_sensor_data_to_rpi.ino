

#include <Arduino.h>
#include <EEPROM.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <ESP8266WiFi.h>

// Define SPI Pins
#define CS_PIN 4 // D2
#define MOSI_PIN 13 // D7 
#define MISO_PIN 12 // D6
#define SCK_PIN 14 // D5

// ADXL345 Registers
#define POWER_CTL 0x2D
#define DATA_FORMAT 0x31
#define DATAX0 0x32
#define DATAX1 0x33
#define DATAY0 0x34
#define DATAY1 0x35
#define DATAZ0 0x36
#define DATAZ1 0x37


// Network and MQTT settings
const char* ssid = "TP-Link_2CCB";
const char* password = "42210665";
const char* mqtt_server = "192.168.0.10";

// NTP Client settings
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 0, 60000);

// Scaling factor for converting to g for ±4g sensitivity
const float scale = 1.0 / 256.0;
int X_offset = 0, Y_offset = 0, Z_offset = 0;

// WiFi and MQTT Clients
WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(9600);
  EEPROM.begin(512); // Initialize EEPROM area with needed size
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  pinMode(MOSI_PIN, OUTPUT);
  pinMode(MISO_PIN, INPUT);
  pinMode(SCK_PIN, OUTPUT);
  pinMode(CS_PIN, OUTPUT);

  digitalWrite(CS_PIN, HIGH); // Deselect the ADXL345
  initADXL345(); // Initialize ADXL345 settings
  loadOffsets(); // Load the saved offsets from EEPROM

  timeClient.begin();
}

void loop() { 
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  static unsigned long lastSendTime = 0;
  if (millis() - lastSendTime > 1000) {
    int x, y, z;
    readAccel(&x, &y, &z);
    float x_dynamic = (x - X_offset) * scale;
    float y_dynamic = (y - Y_offset) * scale;
    float z_dynamic = (z - Z_offset) * scale - 1.0;
    float total_acceleration = sqrt(x_dynamic * x_dynamic + y_dynamic * y_dynamic + z_dynamic * z_dynamic);
    Serial.print(x_dynamic);
    Serial.print(" ");
    Serial.print(y_dynamic);
    Serial.print(" ");
    Serial.print(z_dynamic);
    Serial.print(" ");
    Serial.println(total_acceleration,3);
    timeClient.update();
    String timestamp = timeClient.getFormattedTime();
    String payload = "{\"module_id\": \"sensor2-esp\", \"gps_location\": \"46.638863, 21.50277\", \"acceleration\": \"" + String(total_acceleration, 3) + "\", \"timestamp\": \"" + timestamp + "\"}";
    bool result = client.publish("sensor2/data", payload.c_str());
    if (!result) {
        Serial.println("Publish failed");
    } else {
        Serial.println("Publish successful");
    }
    lastSendTime = millis();
  }
}
void readAccel(int *x, int *y, int *z) {
  *x = readSPIRegister16(DATAX0);
  *y = readSPIRegister16(DATAY0);
  *z = readSPIRegister16(DATAZ0);
}


void setup_wifi() {
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
}
void reconnect() {
  while (!client.connected()) {
    if (client.connect("ESP8266Client")) {
      client.publish("outTopic", "hello world");
      client.subscribe("inTopic");
    } else {
      delay(5000);
    }
  }
}

void initADXL345() {
  writeSPIRegister(DATA_FORMAT, 0x0B); // Set to ±4g range
  writeSPIRegister(POWER_CTL, 0x08); // Measurement mode
  // calibrateADXL345();
}
void calibrateADXL345() {
  const int numReadings = 500;
  int targetValue = 256; 

  Serial.println("Calibrating X-axis. Place X upward...");
  delay(20000); 
  calibrateAxis(DATAX0, &X_offset, true);

  Serial.println("Calibrating Y-axis. Place Y upward...");
  delay(20000);
  calibrateAxis(DATAY0, &Y_offset, true);

  Serial.println("Calibrating Z-axis. Place Z upward...");
  delay(20000);
  calibrateAxis(DATAZ0, &Z_offset, true);

  saveOffsets();
}
void calibrateAxis(byte startReg, int *offset, bool isUp) {
  int sum = 0;
  for (int i = 0; i < 10000; i++) {
  sum += readSPIRegister16(startReg);
  }
  *offset = sum / 10000 - (isUp ? 256 : 0); 
}

int readSPIRegister16(byte reg) {
  digitalWrite(CS_PIN, LOW);
  spiTransfer(reg | 0xC0); // Set read and MB bits
  int val = (int16_t)((spiReceive() | (spiReceive() << 8))); // Convert to signed int correctly
  digitalWrite(CS_PIN, HIGH);
  return val;
}

void writeSPIRegister(byte reg, byte value) {
  digitalWrite(CS_PIN, LOW);
  spiTransfer(reg); // Register address
  spiTransfer(value); // Register value
  digitalWrite(CS_PIN, HIGH);
}

byte spiTransfer(byte data) {
  byte inData = 0;
  for (int i = 7; i >= 0; i--) {
  digitalWrite(SCK_PIN, LOW);
  digitalWrite(MOSI_PIN, (data >> i) & 1);
  digitalWrite(SCK_PIN, HIGH);
  inData |= (digitalRead(MISO_PIN) << i);
  }
  return inData;
}

byte spiReceive() {
  return spiTransfer(0x00); // Send dummy byte to receive data
}


void saveOffsets() {
  EEPROM.put(0, X_offset); 
  EEPROM.put(sizeof(int), Y_offset); 
  EEPROM.put(2 * sizeof(int), Z_offset); 
  EEPROM.commit(); // Ensure data is written to EEPROM

  // Debug output to check values
  Serial.print("Loaded X_offset: ");
  Serial.println(X_offset);
  Serial.print("Loaded Y_offset: ");
  Serial.println(Y_offset);
  Serial.print("Loaded Z_offset: ");
  Serial.println(Z_offset);
}
void loadOffsets() {
  EEPROM.get(0, X_offset);
  EEPROM.get(sizeof(int), Y_offset); 
  EEPROM.get(2 * sizeof(int), Z_offset); 
}