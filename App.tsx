/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  Alert,
  Button,
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import MaterialIcons from '@react-native-vector-icons/material-icons';

import { Amplify } from 'aws-amplify';
import { generateClient } from 'aws-amplify/api';
import { Authenticator, useAuthenticator,  useTheme, defaultDarkModeOverride, ThemeProvider, } from '@aws-amplify/ui-react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AccountScreen from './src/screens/AccountScreen';
import PastEarthquakesScreen from './src/screens/PastEarthquakeScreen';
import config from './src/amplifyconfiguration.json';
import { onSensorDataUpdated } from './src/graphql/subscriptions';

import { EarthquakeDataProvider } from './src/context/EarthquakeDataContext';
import { useEarthquakeData } from './src/context/EarthquakeDataContext';
import MyAppHeader from './MyAppHeader';
import getTheme from './AppAuthTheme';
import { LogLevel, OneSignal } from 'react-native-onesignal';
import CustomSignIn from './CustomSignIn';

import AsyncStorage from '@react-native-async-storage/async-storage';


Amplify.configure(config);
const Tab = createBottomTabNavigator();
const client = generateClient();
const Stack = createNativeStackNavigator();

// Importing Image assets
const crisisAlertIcon = require('./assets/icons/crisis.png');
const historyIcon = require('./assets/icons/history.png');



const options = {
  method: 'POST',
  headers: {accept: 'application/json', 'content-type': 'application/json'}
};
// Create a custom dark theme

const headerImage = require('./assets/icons/header.png');
  



// OneSignal.addEventListener('received', onReceived);
// OneSignal.addEventListener('opened', onOpened);
// OneSignal.addEventListener('ids', onIds);

// Define a custom dark theme
const darkTheme = {
  name: 'dark-theme',
  tokens: {
    components: {
      button: {
        primary: {
          backgroundColor: { value: '#292929' }, // Custom button color
          borderColor: { value: '#FFA500' },
          color: { value: '#ffffff' },
        },
      },
    },
    colors: {
      background: {
        primary: { value: '#292929' },
      },
      font: {
        primary: { value: '#ffffff' },
      },
      brand: {
        primary: { value: '#FFA500' },
      },
    },
  },
};
const formFields = {
  signIn: {
    username: {
      labelHidden: false,
      placeholder: 'Enter your username here',
      isRequired: true,
      label: 'Username:'
    },
  },
  signUp: {      
    email: {     
      order: 3 
    },
    password: {
      order: 1
    },
    confirm_password: {
      order: 2
    },
  }
}
function SignOutButton() {
  const { signOut } = useAuthenticator();
  return <Button title="Sign Out" onPress={signOut} />;
}

const sendPushNotification = async (intensity: any, distance: any) => {
  const playerId = await AsyncStorage.getItem('OneSignalPlayerID');
  if (!playerId) {
    console.error("No Player ID found");
    return;
  }
  const notification = {
    app_id: "8c1c1154-1c3c-44c1-abfb-9bd3e3f99f6b", 
    headings: { en: "Earthquake detected" },
    contents: { en: `Intensity: ${intensity}\nDistance: ${distance} km` },
    include_player_ids: [playerId],
  };

  try {
    const response = await fetch('https://onesignal.com/api/v1/notifications', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Basic NjQwMmYxYzEtNjg2Yi00YWFkLTk0YTgtYTFhZWE0MDBhODEx', // Ensure this is correct and use 'Basic' for authorization header
      },
      body: JSON.stringify(notification),
    });

    const result = await response.json();
    console.log('HTTP Response Status:', response.status);
    if (response.ok) {
      console.log('Notification sent successfully:', result);
    } else {
      console.error('Failed to send notification:', result);
    }
  } catch (error) {
    console.error('Error sending notification:', error);
  }
};


function AppContainer() {
  // const { earthquakeData } = useEarthquakeData(); // Access earthquake data from context
  const { earthquakeData, setEarthquakeData } = useEarthquakeData();


  useEffect(() => {


    if (earthquakeData) {
      // console.log('Updated Earthquake Data:', earthquakeData); // Log the earthquake data when it updates
      console.log(earthquakeData)
      sendPushNotification(earthquakeData.type, earthquakeData.distance)
      // sendPushNotification2();

    }
    // OneSignal.initialize("8c1c1154-1c3c-44c1-abfb-9bd3e3f99f6b");
    // OneSignal.Notifications.requestPermission(true);
    // OneSignal.initialize("8c1c1154-1c3c-44c1-abfb-9bd3e3f99f6b");
    // OneSignal.Notifications.requestPermission(true);

  }, [earthquakeData, setEarthquakeData]);

  
  return (
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let icon;
              if (route.name === 'Account') {
                icon = crisisAlertIcon;
              } else if (route.name === 'Past') {
                icon = historyIcon;
              }
              return <Image source={icon} style={{ width: size, height: size, tintColor: color, opacity: 1 }} />;
            },
            tabBarActiveTintColor: '#FFA500', // Yellow-orange color when a tab is active
            tabBarInactiveTintColor: 'gray', // Color when a tab is inactive
            tabBarStyle: {
              backgroundColor: '#1A1A1A', // Set the tab bar background to black
            },
            
          })}
          
        >
          <Tab.Screen name="Account" component={AccountScreen} options={{  headerShown: false }} />
          <Tab.Screen name="Past" component={PastEarthquakesScreen} options={{ headerShown: false }} />
        </Tab.Navigator>
      </NavigationContainer>
  );
}

// Wrap the entire app in the ThemeProvider and Authenticator
export default function App() {
    // Remove this method to stop OneSignal Debugging
  OneSignal.Debug.setLogLevel(LogLevel.Verbose);

  // OneSignal Initialization
  OneSignal.initialize("8c1c1154-1c3c-44c1-abfb-9bd3e3f99f6b");

  // requestPermission will show the native iOS or Android notification permission prompt.
  // We recommend removing the following code and instead using an In-App Message to prompt for notification permission
  OneSignal.Notifications.requestPermission(true);

  // Method for listening for notification clicks
  OneSignal.Notifications.addEventListener('click', (event) => {
    console.log('OneSignal: notification clicked:', event);
  });
  OneSignal.User.addEventListener('change', (event) => {
    console.log('OneSignal: user changed: ', event);
  });

  OneSignal.User.pushSubscription.addEventListener(
    'change',
    async (subscription) => {
      console.log('OneSignal: subscription changed:', subscription);
      if (subscription.current && subscription.current.id) {
        // Store the player ID in AsyncStorage for later use
        await AsyncStorage.setItem('OneSignalPlayerID', subscription.current.id);
        console.log("Stored new OneSignal Player ID:", subscription.current.id);
      }

    },
  );


  


 
  const {
    tokens: { colors },
  } = useTheme();
  const myTheme = darkTheme

  // Custom Header Component
  const CustomHeader = () => (
    <View style={styles.headerContainer}>
      <Image source={headerImage} style={styles.headerImage} />
    </View>
  );
  return (
    <ThemeProvider theme={myTheme}>
      <Authenticator.Provider
        >
        <Authenticator 
            Header={MyAppHeader}
        >
          <EarthquakeDataProvider>
              <AppContainer />
          </EarthquakeDataProvider>
        </Authenticator>
      </Authenticator.Provider>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerImage: {
    width: 100,
    height: 50,
    resizeMode: 'contain',
  },
});