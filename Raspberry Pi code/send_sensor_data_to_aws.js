
const mqtt = require('mqtt');
const awsIot = require('aws-iot-device-sdk');

// Local MQTT Broker Setup
const localClient = mqtt.connect('mqtt::192.168.0.10');

// AWS IoT Core MQTT Setup
const awsClient = awsIot.device({
    keyPath: '/home/pi/certs/1a399c76d836a78a1e189b6a6f0330f291d60f4f4e6e4b4563ba6ebe375ff718-private.pem.key',
    certPath: '/home/pi/certs/1a399c76d836a78a1e189b6a6f0330f291d60f4f4e6e4b4563ba6ebe375ff718-certificate.pem.crt',
    caPath: '/home/pi/certs/AmazonRootCA1.pem',
    clientId: 'my_rpi_client',
    host: 'a2bi9qysga6fgi-ats.iot.eu-west-1.amazonaws.com'
});

// Threshold for triggering an alert
const THRESHOLD = 0.27; // Example threshold for significant acceleration

// Handling incoming data
localClient.on('connect', () => {
    console.log('Connected to local MQTT broker.');
    localClient.subscribe('sensor1/data');
    localClient.subscribe('sensor2/data');
    localClient.subscribe('sensor3/data');
});

localClient.on('message', (topic, message) => {
    const data = JSON.parse(message.toString());
    if (data.acceleration >= THRESHOLD) {
        console.log(`Threshold exceeded by ${topic}: ${message}`);
        sendToAWS(data, topic);
    }
});

// Function to send data to AWS IoT Core
function sendToAWS(data, topic) {
    const payload = JSON.stringify({
        sensor_id: topic,
        data: data
    });
    
    awsClient.publish('sensor-data', payload, { qos: 1 }, (err) => {
        if (err) {
            console.log(`Failed to send data to AWS for ${topic}:`, err);
        } else {
            console.log(`Data sent to AWS IoT Core for ${topic}:`, payload);
        }
    });
}

awsClient.on('connect', () => {
    console.log('Connected to AWS IoT Core.');
});

// Error handling
localClient.on('error', (error) => {
    console.log('Local MQTT Client Error:', error);
});
awsClient.on('error', (error) => {
    console.log('AWS IoT Client Error:', error);
});
