/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onSensorDataUpdated = /* GraphQL */ `
  subscription OnSensorDataUpdated {
    onSensorDataUpdated {
      package_id
      gps_location
      acceleration
      timestamp
      __typename
    }
  }
`;
