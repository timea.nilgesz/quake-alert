import { Text, View, Image } from 'react-native';
import {
  useTheme,
} from '@aws-amplify/ui-react-native';

const MyAppHeader = () => {
  const {
    tokens: { space, fontSizes },
  } = useTheme();
  return (
    <View style={{ paddingBottom: space.xxl, alignItems: 'center', justifyContent: 'center' }}>
      <Image
        source={require('./assets/icons/header.png')} // Make sure to replace this with the correct path
        style={{ width: 300, height:100, resizeMode: 'contain' }} // Adjust the size as needed
      />
    </View>
  );
};
export default MyAppHeader