import { useTheme } from '@aws-amplify/ui-react-native';
function getTheme() {
  const {
    tokens: { colors },
  } = useTheme();
  const theme = {
    name: 'Custom Theme',
    tokens: {
      colors: {
        background: {
          primary: {
            value: colors.green['100'],
          },
          secondary: {
            value: colors.red['100'],
          },
        },
        font: {
          primary: colors.white,
          secondary: colors.orange['10'],
          interactive: {
            value: colors.orange['80'],
          },
        },
        brand: {
          primary: {
            '10': colors.purple['10'],
            '80': colors.purple['80'],
            '90': colors.purple['90'],
            '100': colors.purple['100'],
          },
        },
      },
    },
  };
  return theme
}

export default getTheme